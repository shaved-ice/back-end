package main

import (
	"fmt"
	"se_backend/config"

	"github.com/gofiber/fiber/v2"
)

func main() {
	config.Init()
	config := config.GetConfig()

	app := fiber.New()

	app.Listen(fmt.Sprintf(":%d", config.GetInt("server.port")))
}
