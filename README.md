# SE Project (PlanMe) Backend

---

### How to setup

1. Clone this repository
2. Run these commands

```console
$ cp config.example.yaml config.yaml
```

3. Edit your config file
4. Run the program

```console
$ go get
$ go run main.go
```
